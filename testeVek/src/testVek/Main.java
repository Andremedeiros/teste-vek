package testVek;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class Main extends JFrame {
	
	private String lista[]=new String [6];
	private String pag;
	Object[] tipoPag = {"D�bito", "Cr�dito", "Cr�dito/Parcelado", "Parcelado"};

	private JPanel contentPane;
	
	public void init() {
		setTitle("Teste Vek");
		setVisible(true);
	}
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main frame = new Main();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Main(){
		init();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 462, 363);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JComboBox cBox = new JComboBox();
		cBox.setFont(new Font("Tahoma", Font.BOLD, 20));
		cBox.setModel(new DefaultComboBoxModel(new String[] {"1318", "4409", "4628", "4430"}));
		cBox.setBounds(163, 83, 108, 47);
		cBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e){
				if(cBox.getSelectedItem().equals("4409")) {
					 pag = (String) JOptionPane.showInputDialog(null, "Selecione a forma de pagamento", "Forma de Pagamento", JOptionPane.PLAIN_MESSAGE, null, tipoPag, null);                
				        JOptionPane.showMessageDialog(null, "Pagamento Realizado", pag, JOptionPane.PLAIN_MESSAGE); 
				}else if(cBox.getSelectedItem().equals("4628")) {
					pag = (String) JOptionPane.showInputDialog(null, "Selecione a forma de pagamento", "Forma de Pagamento", JOptionPane.PLAIN_MESSAGE, null, tipoPag, null);
			        
			        		try {
								Execucao.verificarPagamento(pag, lista);
							} catch (ParseException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						
						
					
				}else if(cBox.getSelectedItem().equals("4430")) {
					JFormattedTextField amex = new JFormattedTextField();
					amex.equals(lista[3]);
					try {
						Execucao.mascara(amex);
					} catch (ParseException e1) {
						
						e1.printStackTrace();
					}	
					
					
					JOptionPane.showMessageDialog(null, null, null, JOptionPane.INFORMATION_MESSAGE);
					String tAmex; 
					tAmex = amex.getText();
					tAmex = tAmex.replace(" ", "");
					Execucao.opc = 3;
					
						try {
							Execucao.checkValorVazio(tAmex, pag, lista);
						} catch (ParseException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					
			}
			}
		});
		
		
		contentPane.add(cBox);
		
		JButton txtValoresTaxa = new JButton("Inserir valores de taxa de Parcelamento");
		txtValoresTaxa.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				JFormattedTextField taxaParcelamento = new JFormattedTextField();
				JFormattedTextField taxaParcelamento6 = new JFormattedTextField();
				JFormattedTextField taxaParcelamento12 = new JFormattedTextField();
				
				
				
				Object [] local = {"Informe o valor da Taxa de Parcelamento", taxaParcelamento,
						"Informe o valor da Taxa M�nima de Parcelamento em 6x", taxaParcelamento6,
						"Informe o valor da Taxa M�nima de Parcelamento em 12x", taxaParcelamento12,};
				JOptionPane.showMessageDialog(null, local, "Taxas", JOptionPane.INFORMATION_MESSAGE);
				
				if(taxaParcelamento.getText()!="" && taxaParcelamento.getText()!=null) {
					lista[0] = taxaParcelamento.getText();
				}
				
				if(taxaParcelamento6.getText()!="" && taxaParcelamento6.getText()!=null) {
					lista[1] = taxaParcelamento6.getText();
				}
				
				if(taxaParcelamento12.getText()!="" && taxaParcelamento12.getText()!=null) {
					lista[2] = taxaParcelamento12.getText();
				}
				
				
				
				
			}
		});
		txtValoresTaxa.setBounds(100, 153, 258, 38);
		contentPane.add(txtValoresTaxa);
		
		JButton btnNewButton = new JButton("Inserir Taxas de Cr�dito e Amex");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFormattedTextField taxaAmex = new JFormattedTextField();
				JFormattedTextField taxaMinCredito = new JFormattedTextField();
				JFormattedTextField taxaMaxCredito = new JFormattedTextField();
				
				Object[] local = {"Informe o valor da Taxa M�nima de Cr�dito",taxaMinCredito,"Informe o valor da Taxa M�xima de Cr�dito",
						taxaMaxCredito,"Informe o valo da Taxa Amex",taxaAmex};
				JOptionPane.showMessageDialog(null, local, "Taxas", JOptionPane.INFORMATION_MESSAGE);
				
				if(taxaAmex.getText() != ""&&taxaAmex.getText() != null) {
					lista[3] = taxaAmex.getText();
				}
				if(taxaMinCredito.getText()!=""&&taxaMinCredito.getText()!=null) {
					lista[4] = taxaMinCredito.getText();
				}
				if(taxaMaxCredito.getText()!=""&&taxaMaxCredito.getText()!=null) {
					lista[5] = taxaMaxCredito.getText();
				}
				setVisible(false);
				Main principal = new Main();
				principal.setVisible(true);
			}
		});
		btnNewButton.setBounds(100, 202, 258, 38);
		contentPane.add(btnNewButton);
		
	}
}
