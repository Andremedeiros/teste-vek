package testVek;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.text.MaskFormatter;

public class Execucao extends Main{

	static int opc;
	static int valor;
	
	
	public static void verificarPagamento(String pag, String lista[]) throws ParseException {
		JFormattedTextField parcela = new JFormattedTextField();
		
		
		if(pag.equals("D�bito")) {
			JOptionPane.showMessageDialog(null, "Pagamento Realizado", null, JOptionPane.INFORMATION_MESSAGE);
		}else 
			if(pag.equals("Cr�dito")) {
				
				opc = 1; 
				mascara(parcela);
				
				Object[] local = {"Taxa de Cr�dito", parcela};
				
				JOptionPane.showMessageDialog(null, local, pag, JOptionPane.INFORMATION_MESSAGE);
				String parcelamento = parcela.getText();
				parcelamento = parcelamento.replace(" ", "");
				checkValorVazio(parcelamento, pag, lista);	
			}else {
				opc = 2;
				
				
				
				String parcelamento6x = String.valueOf(lista[1]);
				String parcelamento12x = String.valueOf(lista[2]);
				checkParcelaVazia(parcelamento6x, parcelamento12x, pag, lista);
				
				}
		}
	
	
	
	
	
	public static void checkValorVazio(String valorVazio, String pag, String lista[]) throws ParseException {
		if(valorVazio.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Campo Obrigat�rio, favor preencher", "Aviso", JOptionPane.WARNING_MESSAGE);
			
		}else {
			valor = Integer.parseInt(valorVazio);
			verificarPagamento(pag, lista);
		}	
	}
	
	public static void checkParcelaVazia(String parcelamento6x, String parcelamento12x, String pag, String lista[]) {
		if(!parcelamento6x.isEmpty()) {
			valor = Integer.parseInt(parcelamento6x);
			checkValor(pag, lista);
			parcelamento12x = "";
		}else if(!parcelamento12x.isEmpty()) {
			if(parcelamento12x.equalsIgnoreCase("N/A")) {
				JOptionPane.showMessageDialog(null, "Pagamento Conclu�do", pag, JOptionPane.INFORMATION_MESSAGE);
			}else {
				valor = Integer.parseInt(parcelamento12x);
				checkValor(pag, lista);
			}
			
		}
	}
	
	public static void checkValor(String pag, String lista[]) {
		int taxaMin = Integer.parseInt(String.valueOf(lista));
		int taxaMax = Integer.parseInt(String.valueOf(lista[5]));
		if(opc == 3) {
			if(valor >=taxaMin && valor <=taxaMax) {
				JOptionPane.showMessageDialog(null, "Pagamento Realizado", pag, JOptionPane.INFORMATION_MESSAGE);
			}else {
				JOptionPane.showMessageDialog(null, "Taxa informada encontra-se fora dos valores aceit�veis", "Erro", JOptionPane.ERROR_MESSAGE);
			}
		}else if(valor > taxaMax) {
			JOptionPane.showMessageDialog(null, "Pagamento Realizado", null, JOptionPane.INFORMATION_MESSAGE);
		}else {
			JOptionPane.showMessageDialog(null, "Taxa informada encontra-se fora dos valores aceit�veis", "Erro", JOptionPane.ERROR_MESSAGE);
		}
	}

	
	
	
	public static void mascara(JFormattedTextField txtfield) throws ParseException {
		try {	
			MaskFormatter numero = new MaskFormatter("**");
			numero.setValidCharacters("0123456789");
			numero.install(txtfield);
			txtfield.addFocusListener(new FocusListener() {
				
				@Override
				public void focusLost(FocusEvent e) {
					txtfield.setCaretPosition(0);
					
				}
				
				@Override
				public void focusGained(FocusEvent e) {
					txtfield.setCaretPosition(0);
					
				}
			});
		}catch (ParseException ex1) {
			Logger.getLogger(Execucao.class.getName()).log(Level.SEVERE, null, ex1);
		}
	}
}
